
README

CheckMate

CheckMate è un'applicazione creata per simulare il comune e famoso gioco degli scacchi. il software 
presenta e segue fedelmente il regolamento di tale gioco.

How do i get set up?

Prima di iniziale una partita, è possibile settare lo stile delle pedine e il colore della scacchiera, oltre
a poter scegliere se giocare una partita a tempo o meno. Durante la partita è possibile giocare in modalità facilitata,
scegliendo se visualizzare le pedine minacciate e/o le possibili posizioni in cui si può muovere ciascuna pedina.

Who do I talk to?

    Bacchilega Giulio: giulio.bacchilega@studio.unibo.it
    Farneti Linda: linda.farneti@studio.unibo.it
    Maglieri Giulia: giulia.maglieri@studio.unibo.it
    Ravaglia Alex: alex.ravaglia@studio.unibo.it

