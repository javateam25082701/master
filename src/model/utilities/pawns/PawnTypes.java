package model.utilities.pawns;

/**
 * @author : Giulio Bacchilega
 */
public enum PawnTypes {
    /**
     * 
     */
    King, Queen, Knight, Bishop, Rook, SimplePawn;
}
