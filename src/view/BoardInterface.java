package view;

import javafx.stage.Stage;
/**
 * 
 * Author Giulia Maglieri.
 *
 */
public interface BoardInterface {
    /**
     * 
     */
    void start();
    /**
     * 
     * @return Stage
     */
    Stage getStage();
}
